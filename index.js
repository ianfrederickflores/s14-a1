let firstName;
firstName = "John";
let lastName;
lastName = "Smith";
let age;
age = 30;
console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:")
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);
console.log("Work Address:");
let workAddress = {
	city: "Lincoln",
	houseNumber: "32",
	state: "Nebraska",
	street: "Washington"
}
console.log(workAddress);
function createMessage(firstName,lastName, age){
	console.log(firstName + " " + lastName + " is " + age + " years of age.");
}
createMessage("John", "Smith", 30)
function argumentFunction(){
	console.log("This was printed inside of the function");
}
function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);
function hobbiesAgain(){
	console.log([
		"Biking",
		"Mountain CLimbing",
		"Swimming",
	])
}
hobbiesAgain()
invokeFunction(argumentFunction);
function workAddressAgain(){
	console.log({
		city: "Lincoln",
		houseNumber: "32",
		state: "Nebraska",
		street: "Washington"
	})
}
workAddressAgain()
function returnIsMarried(isMarried = true){
	return "The value of isMarried is: " + isMarried
}
console.log(returnIsMarried(isMarried = true))